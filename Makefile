all : clean netfilter_block

netfilter_block: netfilter_block.o
	g++ -g -o netfilter_block netfilter_block.o -lnetfilter_queue

netfilter_block.o:
	g++ -std=c++14 -g -c -o netfilter_block.o nfqnl_test.cpp

clean:
	rm -rf *.o
